//author: David Fontenot
//date: April, 2014

package org.ai.eighttile;

import org.ai.eighttile.EightTile;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Hashtable;
import java.lang.Boolean;

public class BFS extends org.ai.eighttile.Searcher {
    private class Node {
        private Node parent;
        private String state;

        public Node(String s, Node p) {
            state = s;
            parent = p;
        }

        public Node getParent() {
            return parent;
        }

        public String getState() {
            return state;
        }
    }

    public BFS(String input, String goal) {
        super(input, goal);
    }

    public void start() {
        Hashtable<String, Boolean> closed = new Hashtable<String, Boolean>();
        ArrayList<String> options = new ArrayList<String>();
        LinkedList<Node> frontier = new LinkedList<Node>();
        Node lastState = new Node(input, null);

        if(input.equals(goal))
            found = true;       // already solved

        closed.put(input, new Boolean(true));
        frontier.addLast(lastState);
        options.addAll(EightTile.options(input));

        while(!found) {
            // expand all of the children of this node
            for(String opt : options) {
                if(opt.equals(goal)) {
                    frontier.addFirst(new Node(opt, lastState)); // add this node so the path can be drawn
                    found = true;
                    break;      // skip the rest of the processing code
                }

                if(!closed.containsKey(opt)) { // add this node to the frontier if it hasn't been seen yet
                    frontier.addFirst(new Node(opt, lastState));
                    closed.put(opt, new Boolean(true));
                }
            }

            if(frontier.isEmpty()) {
                return;         // solution not found
            }

            if(!found) {        // found could be set to true above
                lastState = frontier.removeLast();
                numExpanded++;
                options.clear();
                options.addAll(EightTile.options(lastState.getState()));
            }
        }

        // move from solution to the input state and add to path
        Node n = frontier.pollFirst(); // last item inserted should be the goal state
        path.addFirst(n.getState());
        while(n.getParent() != null) {
            n = n.getParent();
            path.addFirst(n.getState());
        }
    }
}
