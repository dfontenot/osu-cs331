//author: David Fontenot
//date: April, 2014

package org.ai.eighttile;

import org.ai.eighttile.EightTile;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Hashtable;
import java.lang.Boolean;
import java.util.PriorityQueue;

public class AStar extends org.ai.eighttile.Searcher {
    private class AStarNode implements java.lang.Comparable {
        private String state;
        private String goal;
        private int distFromStart;
        private AStarNode parent;

        public AStarNode(String conf, String g, int dist, AStarNode p) {
            parent = p;
            goal = g;
            state = conf;
            distFromStart = dist;
        }

        public AStarNode getParent() { return parent; }

        public String getState() { return state; }

        public int getDist() { return distFromStart; }

        // heuristic being used is how many numbers are out of place
        public int h() {
            char gChars[] = goal.toCharArray();
            char curChars[] = state.toCharArray();

            int accum = 0;
            for(int i = 0; i < EightTile.BOARD_SZ; i++) {
                if(gChars[i] != curChars[i])
                    accum++;
            }

            return accum;
        }

        public int g() {
            return distFromStart;
        }

        public int compareTo(Object o) {
            AStarNode other = (AStarNode)o;
            int otherScore = other.g() + other.h();

            return java.lang.Integer.compare(g() + h(), otherScore);
        }

        public boolean equals(Object o) {
            AStarNode other = (AStarNode)o;

            return g() + h() == other.g() + other.h();
        }
    }

    public AStar(String input, String goal) {
        super(input, goal);
    }

    public void start() {
        Hashtable<String, Boolean> closed = new Hashtable<String, Boolean>();
        ArrayList<String> options = new ArrayList<String>();
        PriorityQueue<AStarNode> frontier = new PriorityQueue<AStarNode>();
        int curDist = 0;
        AStarNode curNode = new AStarNode(input, goal, curDist, null); // current node being evaluated

        if(input.equals(goal))
            found = true;       // already solved

        closed.put(input, new Boolean(true));
        frontier.add(curNode);
        options.addAll(EightTile.options(input));

        while(!found) {
            curDist = frontier.peek().getDist();

            for(String opt : options) {
                if(opt.equals(goal)) {
                    found = true;
                    frontier.add(new AStarNode(opt, goal, curDist + 1, curNode)); // this node will be polled below
                    break;
                }

                if(!closed.containsKey(opt)) { // put on frontier
                    closed.put(opt, new Boolean(true));
                    frontier.add(new AStarNode(opt, goal, curDist + 1, curNode));
                }
            }

            if(frontier.size() == 1) { // no solution
                return;
            }

            if(!found) {
                curNode = frontier.poll(); // pop off the most likely node to be closest to the solution
                numExpanded++;             // update (only for nodes that are polled from the frontier)
                options.clear();
                options.addAll(EightTile.options(curNode.getState()));
            }
        }

        // trace the path from the goal to the input
        AStarNode n = frontier.poll();
        path.addFirst(n.getState());
        while(n.getParent() != null) {
            n = n.getParent();
            path.addFirst(n.getState());
        }
    }
}
