//author: David Fontenot
//date: April, 2014

package org.ai.eighttile;

import java.util.Stack;
import java.util.Hashtable;
import java.util.ArrayList;

public class IDDFS extends org.ai.eighttile.Searcher {
    private Hashtable<String, Stack<String>> closed;

    public IDDFS(String input, String goal) {
        super(input, goal);
    }

    public void start() {
        closed = new Hashtable<String, Stack<String>>();
        ArrayList<String> options = new ArrayList<String>();
        Stack<String> frontier = new Stack<String>();
        Stack<String> cloned = null; // explicitely clone to avoid warnings from the linter

        int depthLimit = 2;
        int depth = 1;
        int depthMax = 1;       // used to determine when there is no solution, if depthmax is less than the depth then no solution

        if(input.equals(goal))
            found = true;       // input is the solution

        boolean currentDepthExplored = false;
        boolean optionSelected = false;
        while(!found) {

            closed.put(input, frontier);
            frontier.push(input);
            options.addAll(EightTile.options(input));

            // continue to look while constrained to the current depthLimit
            while(!currentDepthExplored) {

                for(String opt : options) { // if options is populated, that means the depth can go further
                    if(opt.equals(goal))
                        found = true;

                    if(closed.containsKey(opt)) {
                        if(closed.get(opt).size() > depth + 1) {           // is there a better path to this option?
                            System.exit(0);
                            frontier.push(opt);                            // go over this node again
                            options.clear();
                            options.addAll(EightTile.options(opt));

                            cloned = new Stack<String>();
                            cloned.addAll(frontier);
                            closed.remove(opt);                            // remove state
                            closed.put(opt, cloned);                       // update the cheapest path to this node
                            optionSelected = true;
                            break;
                        }
                    }
                    else {
                        frontier.push(opt);                                // go here next
                        options.clear();
                        options.addAll(EightTile.options(opt));
                        cloned = new Stack<String>();
                        cloned.addAll(frontier);
                        closed.put(opt, cloned);                           // so far this is the best path to this new option
                        optionSelected = true;
                        break;
                    }
                }

                if(frontier.size() == 1) {
                    currentDepthExplored = true;
                    break;
                }

                if(optionSelected) {
                    depth++;
                    
                    if(depth > depthMax)
                        depthMax++;
                }

                if(!optionSelected || depth == depthLimit) { // out of options or gone too deep, go up
                    numExpanded++;
                    options.clear();
                    frontier.pop();
                    options.addAll(EightTile.options(frontier.peek()));
                    depth--;
                }

                optionSelected = false;
            } // end inner while

            if(depthMax < depthLimit) // no solution found
                return;

            if(found) {         // build up path and return
                Stack<String> bestRoute = closed.get(goal);
                while(!bestRoute.empty()) {
                    path.addFirst(bestRoute.pop());
                }

                return;         // path has been built, can return
            }

            // reset variables and continue searching
            closed.clear();
            currentDepthExplored = false;
            frontier.clear();
            depth = 1;
            depthMax = 1;
            numExpanded = 0;

            depthLimit++;
        }
    }
}
