//author: David Fontenot
//date: April, 2014

package org.ai.eighttile;

import java.util.LinkedList;

public abstract class Searcher {
    protected String input;
    protected String goal;
    protected boolean found;
    protected LinkedList<String> path;
    protected int numExpanded;

    public Searcher(String i, String g) {
        this.input = i;
        this.goal = g;
        found = false;
        numExpanded = 0;
        path = new LinkedList<String>();
    }

    public abstract void start();

    public int countExpandedNodes() { return numExpanded; }

    public boolean found() { return found; }

    public LinkedList<String> getPath() { return path; }
}
