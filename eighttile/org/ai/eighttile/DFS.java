//author: David Fontenot
//date: April, 2014

package org.ai.eighttile;

import org.ai.eighttile.EightTile;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Hashtable;
import java.lang.Boolean;

public class DFS extends org.ai.eighttile.Searcher {
    public DFS(String input, String goal) {
        super(input, goal);
    }

    public void start() {
        Hashtable<String, Boolean> closed = new Hashtable<String, Boolean>();
        ArrayList<String> options = new ArrayList<String>();

        if(input.equals(goal))
            found = true;       // input is the solution

        closed.put(input, new Boolean(true));
        path.addLast(input);
        options.addAll(EightTile.options(input));

        String curOpt;
        while(!found) {
            curOpt = null;

            for(String opt : options) {
                if(opt.equals(goal)) {
                    curOpt = opt;
                    found = true; // will be added to the path later on
                }

                if(!closed.containsKey(opt)) {
                    curOpt = opt;
                    break;
                }
            }

            if(curOpt == null) { // nothing found
                if(path.size() == 1) {
                    return;     // failed to find a solution
                }
                else {
                    options.clear();              // clear out the options and add new ones
                    options.addAll(EightTile.options(path.removeLast()));
                }
            }
            else {              // new item found
                closed.put(curOpt, new Boolean(true));
                options.clear();
                path.addLast(curOpt);
                numExpanded++;
                options.addAll(EightTile.options(curOpt));
            }
        }
    }
}
