//author: David Fontenot
//date: April, 2014

package org.ai.eighttile;

import java.util.LinkedList;
import java.util.ArrayList;
import java.io.Reader;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.IOException;

public class EightTile {
    public static final int BOARD_SZ = 9;
    public static final int ROW_SZ = BOARD_SZ / 3;

    public static ArrayList<String> options(String curState) {
        ArrayList<String> states = new ArrayList<String>();

        int x = 0;
        int y = 0;
        int index;

        if((index = curState.indexOf('0')) < 0) {
            System.out.println("ERROR\n" + formatState(curState) + " does not contain a zero");
            return null;        // make sure a 0 was actually found
        }
        else {
            if(!(curState.indexOf('0', index + 1) < 0)) {
                System.out.println("ERROR\n" + formatState(curState) + " contains more than 1 zero");
                return null;
            }

            x = index % ROW_SZ;
            y = index / ROW_SZ;
        }

        // the order of moves matters here for the assignment
        if(y != ROW_SZ-1) {     // a tile can go up
            char[] s = curState.toCharArray();
            s[x+(y*ROW_SZ)] = s[x+((y+1)*ROW_SZ)];
            s[x+((y+1)*ROW_SZ)] = '0';
            states.add(new String(s));
        }
        
        if(x != 0) {            // a tile can go right
            char[] s = curState.toCharArray();
            s[x+(y*ROW_SZ)] = s[x+(y*ROW_SZ)-1];
            s[x+(y*ROW_SZ)-1] = '0';
            states.add(new String(s));
        }

        if(y != 0) {            // a tile can go down
            char[] s = curState.toCharArray();
            s[x+(y*ROW_SZ)] = s[x+((y-1)*ROW_SZ)];
            s[x+((y-1)*ROW_SZ)] = '0';
            states.add(new String(s));
        }

        if(x != ROW_SZ-1) {     // a tile can go left
            char[] s = curState.toCharArray();
            s[x+(y*ROW_SZ)] = s[x+(y*ROW_SZ)+1];
            s[x+(y*ROW_SZ)+1] = '0';
            states.add(new String(s));
        }

        return states;
    }

    public static String formatState(String s) {
        String endl = "";
        if(System.getProperty("os.name").toLowerCase().contains("windows"))
            endl = "\r\n";
        else
            endl = "\n";
        return new String(s.substring(0,ROW_SZ) + endl + s.substring(ROW_SZ,ROW_SZ*2) + endl + s.substring(ROW_SZ*2,BOARD_SZ));
    }

    public static void main(String[] argv) {
        if(argv.length != 4) {
            System.out.println("Usage java -jar eighttile.jar <initial_state> <goal_state> <mode> <outfile>");
            System.exit(1);
        }

        String inputFile = argv[0];
        String goalFile = argv[1];
        String searchMethod = argv[2];
        String outFile = argv[3];

        String initial = null;
        String goal = null;

        //http://stackoverflow.com/a/811860/854854
        try {
            //read input file
            InputStream inputIn = new FileInputStream(inputFile);
            Reader inputReader = new InputStreamReader(inputIn, Charset.defaultCharset());
            initial = readInput(new BufferedReader(inputReader));
            inputReader.close();

            //read goal file
            InputStream goalIn = new FileInputStream(goalFile);
            Reader goalReader = new InputStreamReader(goalIn, Charset.defaultCharset());
            goal = readInput(new BufferedReader(goalReader));
            goalReader.close();
        }
        catch(IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
        catch(RuntimeException e2) {
            System.out.println(e2.getMessage());
            e2.printStackTrace();
            System.exit(1);
        }

        Searcher s = null;

        // determine which search algorithm to use
        if(searchMethod.equalsIgnoreCase("dfs")) {
            s = new DFS(initial, goal);
        }
        else if(searchMethod.equalsIgnoreCase("bfs")) {
            s = new BFS(initial, goal);
        }
        else if(searchMethod.equalsIgnoreCase("iddfs")) {
            s = new IDDFS(initial, goal);
        }
        else if(searchMethod.equalsIgnoreCase("astar")) {
            s = new AStar(initial, goal);
        }
        else {
            System.out.println("Search option " + searchMethod + " not allowed");
            System.exit(1);
        }

        s.start();              // could hang here on a puzzle with no solution, 
                                // or by using a poor algorithm (DFS)

        String endl = "";
        if(System.getProperty("os.name").toLowerCase().contains("windows"))
            endl = "\r\n";
        else
            endl = "\n";

        // print out the number of expanded nodes
        System.out.println("Nodes expanded: " + s.countExpandedNodes()+endl);
        System.out.println("Solution path (length " + s.getPath().size() + "):");

        //get the result
        BufferedWriter out;
        try {
            out = new BufferedWriter(new FileWriter(outFile));
            if(s.found()) {
                for(String path : s.getPath()) {
                    out.write(formatState(path)+endl+endl); // write out to the file
                    System.out.println(formatState(path)+endl); // also write to stdout
                }
            }
            else {
                System.out.println("No path found");
            }
            out.close();

            System.exit(0);
        }
        catch(IOException e3) {
            System.out.println(e3.getMessage());
            e3.printStackTrace();
            System.exit(1);
        }
    }

    private static String readInput(Reader buf) throws IOException {
        String s = new String("");
        int ch;
        while((ch = buf.read()) != -1) {
            if((char)ch >= '0' && (char)ch <= '8') {
                if(s.length() <= BOARD_SZ) {
                    s += (char)ch;
                }
                else {
                    throw new RuntimeException("More numbers than allowed in file");
                }
            }
        }

        return s;
    }
}
