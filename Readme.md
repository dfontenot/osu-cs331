Oregon State CS 331: Introduction to Artificial Intelligence
============================================================

Stored here are my various projects for the class

Eighttile
------------------
Eighttile solves those eight tile puzzles that some people have on their coffee tables. There are eight tile and a blank tile on a 3 x 3 grid. The goal is to move the numbers one by one and position them into a particular configuration (normally 1-8 in order). The user can select from the following search algorithms: DFS, BFS, iterative deepening DFS, A*.

Tictac
-----------------
Tictac uses Minimax search to implement an unbeatable computer player. The project comes with a basic GUI (not written by me). The possible opponents are: a human, minimax, or a random player.

Naive Bayes
-----------------
A Bayesian network that predicts whether a fortune cookie message is a wise saying, or a prediction of the future.

**Structure**

In this network, each word is a node in the DAG, and the root node points to all of these nodes. None of the nodes point to each other.