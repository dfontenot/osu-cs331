package edu.oregonstate.eecs.cs331.assn2;

import java.util.ArrayList;

/**
 * This class represents the module for minimax.
 * @author Chris Ventura
 *
 */
public class MiniMax implements Player {
    private static final int UTILITY_LOSE = -1;
    private static final int UTILITY_WIN = 1;
    private static final int UTILITY_DRAW = 0;
    private static final int NEG_INF = -1000;
    private static final int POS_INF = 1000;

    private boolean minimize;   // true if the player is the minimizing player

    /**
     * Constructor
     *
     */
    public MiniMax() {

    }

    /**
     * Returns the next move.
     * @param state The current board state in the game
     * @return The next optimal move
     * 
     * @author David Fontenot
     */
    public Position getNextMove(TicTacToeBoard state) throws Exception {
        Position bestPos = null;// best position to take (found so far)
        int bestUtility;        // the best utility value, corresponds with the bestPos
        int otherPlayer;        // the player id of the other player

        // determine if to minimize or maximize
        if(state.getTurn() == TicTacToeBoard.PLAYER_X) { // x player goes first, maximizing
            otherPlayer = TicTacToeBoard.PLAYER_O;
            bestUtility = MiniMax.NEG_INF;
            minimize = false;
        }
        else {
            otherPlayer = TicTacToeBoard.PLAYER_X;
            bestUtility = MiniMax.POS_INF;
            minimize = true;
        }

        TicTacToeBoard curBoard = (TicTacToeBoard)state.clone();
        int curUtility;
        for(Position pos : MiniMax.getSuccessors(state)) {
            curBoard.setState(pos.row, pos.col, state.getTurn()); // set the successor
            curBoard.setToNextTurn();                             // make it the next player's turn

            if(minimize) {
                curUtility = maxValue(curBoard);

                if(curUtility < bestUtility) {
                    bestUtility = curUtility; // update the best found so far
                    bestPos = pos;
                }
            }
            else {
                curUtility = minValue(curBoard);

                if(curUtility > bestUtility) {
                    bestUtility = curUtility; // update the best found so far
                    bestPos = pos;
                }
            }

            // revert board to previous state
            curBoard.clearState(pos.row, pos.col);
            curBoard.setToNextTurn();
        }

        return bestPos;
    }

    /**
     * @param state The current board state in the game
     * @author David Fontenot
     * 
     * @return The max move
     */
    private int maxValue(TicTacToeBoard state) throws Exception {
        if(state.isGameOver())
            return utility(state);

        TicTacToeBoard curBoard = (TicTacToeBoard)state.clone(); // clone the state

        int bestUtility = MiniMax.NEG_INF;
        int curUtility;
        for(Position pos : MiniMax.getSuccessors(state)) {
            curBoard.setState(pos.row, pos.col, state.getTurn()); // mark the space
            curBoard.setToNextTurn();  // make it the next player's turn

            curUtility = minValue(curBoard);

            if(curUtility > bestUtility)
                bestUtility = curUtility;

            // revert board to previous state
            curBoard.clearState(pos.row, pos.col);
            curBoard.setToNextTurn();
        }

        return bestUtility;
    }

    /**
     * @param state The current board state in the game
     * @author David Fontenot
     * 
     * @return The min move
     */
    private int minValue(TicTacToeBoard state) throws Exception{
        if(state.isGameOver())
            return utility(state);

        TicTacToeBoard curBoard = (TicTacToeBoard)state.clone(); // clone the state

        int bestUtility = MiniMax.POS_INF;
        int curUtility;
        for(Position pos : MiniMax.getSuccessors(state)) {
            curBoard.setState(pos.row, pos.col, state.getTurn()); // mark the space
            curBoard.setToNextTurn();  // make it the next player's turn

            curUtility = maxValue(curBoard);

            if(curUtility < bestUtility)
                bestUtility = curUtility;

            // revert board to previous state
            curBoard.clearState(pos.row, pos.col);
            curBoard.setToNextTurn();
        }

        return bestUtility;
    }

    /**
     * Returns an integer value of the utility of the state for the given player
     * @param state The current board state in the game
     * @return A value of how useful that state is to the current player
     *
     * @author David Fontenot
     */
    private int utility(TicTacToeBoard state) throws Exception {
        int curPlayer;
        int otherPlayer;

        if(minimize) {
            curPlayer = TicTacToeBoard.PLAYER_O;
            otherPlayer = TicTacToeBoard.PLAYER_X;
        }
        else {
            curPlayer = TicTacToeBoard.PLAYER_X;
            otherPlayer = TicTacToeBoard.PLAYER_O;
        }

        if(state.isWin(curPlayer) && !state.isWin(otherPlayer))
            return UTILITY_WIN;
        else if(state.isWin(otherPlayer) && !state.isWin(curPlayer))
            return UTILITY_LOSE;
        else
            return UTILITY_DRAW;
    }

    /**
     * Returns all successors of the current move.
     * @param state The current board state in the game
     * @return All possible next moves
     *
     * @author David Fontenot
     */
    private static ArrayList<Position> getSuccessors(TicTacToeBoard curState) {
        ArrayList<Position> successors = new ArrayList<Position>();

        for(int row = 0; row < TicTacToeBoard.SIZE; row++) {
            for(int col = 0; col < TicTacToeBoard.SIZE; col++) {
                if(curState.getState(row, col) == TicTacToeBoard.BLANK)
                    successors.add(new Position(row, col));
            }
        }

        return successors;
    }

    /**
     * Returns the player type 
     */
    public int getPlayerType() {
        return MINIMAX_PLAYER;
    }

}
