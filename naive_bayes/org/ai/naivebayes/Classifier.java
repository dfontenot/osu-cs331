// author: David Fontenot
// date: May 2014

package org.ai.naivebayes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.StringBuilder;

import java.util.PriorityQueue;
import java.util.Hashtable;
import java.util.ArrayList;

import java.lang.RuntimeException;

public class Classifier {
    private class VocabProbabilities {
        private int vocabSize;
        private int[] vocabExistsWise;
        private int[] vocabExistsPredict;
        private int[] noVocabWise;
        private int[] noVocabPredict;

        public VocabProbabilities(int v) {
            vocabSize = v;

            vocabExistsWise = new int[v];
            for(int i = 0; i < v; i++)
                vocabExistsWise[i] = 0;

            vocabExistsPredict = new int[v];
            for(int i = 0; i < v; i++)
                vocabExistsPredict[i] = 0;

            noVocabWise = new int[v];
            for(int i = 0; i < v; i++)
                noVocabWise[i] = 0;

            noVocabPredict = new int[v];
            for(int i = 0; i < v; i++)
                noVocabPredict[i] = 0;
        }

        /*
         * theClass: conditioning on class 0 or 1?
         * index: what number is this word in the alphabet
         * present: looking for if the word is present or not
         */
        public int getCount(int theClass, int index, boolean present) {
            if(present) {
                if(theClass == 0)
                    return vocabExistsWise[index];
                else
                    return vocabExistsPredict[index];
            }
            else {
                if(theClass == 0)
                    return noVocabWise[index];
                else
                    return noVocabPredict[index];
            }
        }

        // add a new single element in the vector
        public void addOccurance(int index, int present, int theClass) {
            if(present == 0 && theClass == 0)
                noVocabWise[index]++;
            else if(present == 0 && theClass == 1)
                noVocabPredict[index]++;
            else if(present == 1 && theClass == 0)
                vocabExistsWise[index]++;
            else if(present == 1 && theClass == 1)
                vocabExistsPredict[index]++;
            else
                throw new RuntimeException("invalid class (" + theClass + ") and vector (" + present + ") items");
        }
    }

    private String[] vocabulary;                  // vocabulary is ordered
    private Hashtable<String, Boolean> stopWords; // only need to know if a word is a stop word

    // filenames
    String stopWordsFile;
    String trainingDataFile;
    String trainingLabelsFile;
    String testingDataFile;
    String testingLabelsFile;

    // training data
    ArrayList<String> trainingMessages; // temporary storage for all messages in the training data
    int[] trainingLabels;
    int[][] trainingFeatures;           // vectors of all training data

    // testing data
    ArrayList<String> testingMessages; // temporary storage for all messages in the testing data
    int[] testingLabels;
    int[][] testingFeatures;           // vectors of all testing data

    // probabilities
    int numWiseMessages;               // number of wise messages in the training data
    int numPredictMessages;

    private VocabProbabilities vProbs;

    // etc
    static String endl = System.getProperty("os.name").toLowerCase().contains("windows") ? "\r\n" : "\n";
    static Charset defCh = Charset.defaultCharset();

    /* 
     * stop words file should have one word per line
     */
    private void populateStopWords() throws FileNotFoundException, IOException {
        stopWords = new Hashtable<String, Boolean>();

        // http://stackoverflow.com/a/7413900/854854
        InputStream inputIn = new FileInputStream(stopWordsFile);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputIn, defCh));
        String line;
            
        while((line = inputReader.readLine()) != null)
            stopWords.put(line, new Boolean(true));

        inputReader.close();
    }

    /*
     * vocabulary consists of all of the words in the training data, except for stop words
     */
    private void populateVocabulary() throws FileNotFoundException, IOException {
        PriorityQueue<String> vocabularyQueue = new PriorityQueue<String>();
        trainingMessages = new ArrayList<String>();

        InputStream inputIn = new FileInputStream(trainingDataFile);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputIn, defCh));
        String line;
            
        while((line = inputReader.readLine()) != null) {
            trainingMessages.add(line); // store this message to be converted to a feature vector later

            String[] words = line.split(" +"); // each word separated by at least one space
            for(String word : words) {
                if(!stopWords.containsKey(word) && 
                   !vocabularyQueue.contains(word))
                    vocabularyQueue.add(word); // add unique non stop words only
            }
        }

        vocabulary = new String[vocabularyQueue.size()];
        vocabularyQueue.toArray(vocabulary); // write out the vocabulary

        inputReader.close();
    }

    private void outputFeatures() throws FileNotFoundException, IOException {
        // http://stackoverflow.com/a/2885241
        FileOutputStream fOut = new FileOutputStream("preprocessed.txt");
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(fOut, defCh));

        // first line is entire csv vocabulary
        StringBuilder sb = new StringBuilder(); // more efficient than building up string objects directly

        // first line is all vocab words
        sb.append(vocabulary[0]);
        for(int i = 1; i < vocabulary.length; i++)
            sb.append("," + vocabulary[i]);
        sb.append(endl);

        // rest of lines are the feature vectors
        for(int n = 0; n < trainingMessages.size(); n++) {
            sb.append(trainingFeatures[n][0]); // first item in csv list
            for(int l = 1; l < vocabulary.length + 1; l++)
                sb.append("," + trainingFeatures[n][l]);
            sb.append(endl);
        } // leave trailing newline

        // write out to the file
        w.write(sb.toString());
        w.close();
    }

    private void populateTestingFeatures() throws FileNotFoundException, IOException {
        // get the messages first
        testingMessages = new ArrayList<String>();

        InputStream inputIn = new FileInputStream(testingDataFile);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputIn, defCh));
        String line;

        while((line = inputReader.readLine()) != null)
            testingMessages.add(line);

        // set the labels
        ArrayList<Integer> tempInts = new ArrayList<Integer>();
        InputStream inputInLabels = new FileInputStream(testingLabelsFile);
        BufferedReader inputReaderLabels = new BufferedReader(new InputStreamReader(inputInLabels, defCh));

        while((line = inputReaderLabels.readLine()) != null)
            tempInts.add(new Integer(line));

        if(testingMessages.size() != tempInts.size())
            throw new RuntimeException("labels count did not match number of messages");

        // used for verification purposes
        testingLabels = new int[tempInts.size()];
        for(int i = 0; i < testingLabels.length; i++)
            testingLabels[i] = tempInts.get(i).intValue();

        // labels are not stored in this feature
        // provided separately
        int numFeatures = testingMessages.size();
        int lengthFeature = vocabulary.length;

        testingFeatures = new int[numFeatures][lengthFeature];
        for(int n = 0; n < numFeatures; n++) {
            for(int l = 0; l < lengthFeature; l++) {
                // set whether the word is present in the message
                testingFeatures[n][l] = testingMessages.get(n).contains(vocabulary[l]) ? 1 : 0;
            }
        }
    }

    private void populateTrainingFeatures() throws FileNotFoundException, IOException {
        // set the labels first
        ArrayList<Integer> tempInts = new ArrayList<Integer>();
        InputStream inputIn = new FileInputStream(trainingLabelsFile);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputIn, defCh));
        String line;

        while((line = inputReader.readLine()) != null)
            tempInts.add(new Integer(line));

        if(trainingMessages.size() != tempInts.size()) {
            System.out.println("labels count did not match number of messages");
            System.exit(1);
        }

        trainingLabels = new int[tempInts.size()];
        for(int i = 0; i < trainingLabels.length; i++)
            trainingLabels[i] = tempInts.get(i).intValue();

        numWiseMessages = 0;

        int numFeatures = trainingMessages.size();
        int lengthFeature = vocabulary.length;

        trainingFeatures = new int[numFeatures][lengthFeature + 1];
        for(int n = 0; n < numFeatures; n++) {
            for(int l = 0; l < lengthFeature; l++) {
                // set whether the word is present in the message
                trainingFeatures[n][l] = trainingMessages.get(n).contains(vocabulary[l]) ? 1 : 0;
            }

            trainingFeatures[n][lengthFeature] = trainingLabels[n]; // set the label this message should have
            if(trainingLabels[n] == 0)
                numWiseMessages++;
        }

        numPredictMessages = trainingMessages.size() - numWiseMessages;
    }

    // build up vProbs
    private void countTestingOccurances() {
        vProbs = new VocabProbabilities(vocabulary.length);

        for(int n = 0; n < trainingMessages.size(); n++) {
            for(int i = 0; i < vocabulary.length; i++) {
                vProbs.addOccurance(i, trainingFeatures[n][i], 
                                    trainingFeatures[n][vocabulary.length]);
            }
        }
    }

    public Classifier(String stopWords, String trainingData, String trainingLabelsF, 
                      String testingData, String testingLabelsF) {

        // set filenames
        stopWordsFile = stopWords;
        trainingDataFile = trainingData;
        trainingLabelsFile = trainingLabelsF;
        testingDataFile = testingData;
        testingLabelsFile = testingLabelsF;

        try {
            // preprocessing step (training data)
            populateStopWords();          // store all stopwords
            populateVocabulary();         // load all vocab words from input
            populateTrainingFeatures();   // create the feature vectors

            outputFeatures();             // output feature vectors

            // preprocessing step (testing data)
            populateTestingFeatures();    // vocabulary is restricted to the vocab of the training data
        }
        catch(FileNotFoundException e1) {
            System.out.println("FileNotFoundException: " + e1.getMessage());
            e1.printStackTrace();
            System.exit(1);
        }
        catch(IOException e2) {
            System.out.println("IOException: " + e2.getMessage());
            e2.printStackTrace();
            System.exit(1);
        }

        // inference step
        countTestingOccurances();

        int correct = 0;        // correctly predicted messages in the testing data

        // query probabilities
        double wise;                  // class 0
        double predict;               // class 1
        double probNoVocabWise = 0.0; // probability (without wise term) used when no vocab words are found
        double probNoVocabPredict = 0.0;
        boolean found;                // found a vocab word

        // calculate probNoVocab
        for(int i = 0; i < vocabulary.length; i++) {
            probNoVocabWise += Math.log(dirichlet(vProbs.getCount(0, i, false), numWiseMessages));
            probNoVocabPredict += Math.log(dirichlet(vProbs.getCount(1, i, false), numPredictMessages));
        }

        // write out predictions
        try {
            FileOutputStream fOut = new FileOutputStream("results.txt");
            BufferedWriter w = new BufferedWriter(new OutputStreamWriter(fOut, defCh));

            w.write("# training data used: " + trainingDataFile + endl);
            w.write("# training lables used: " + trainingLabelsFile + endl);
            w.write("# testing data used: " + testingDataFile + endl);
            w.write("# testing labels used: " + testingLabelsFile + endl + endl);

            // use the constant e to prevent negative probabilities
            for(int n = 0; n < testingMessages.size(); n++) {
                found = false;

                // use size of training data
                wise = Math.log(((double)numWiseMessages / (double)trainingMessages.size()));
                predict = Math.log(((double)numPredictMessages / (double)trainingMessages.size()));

                for(int i = 0; i < vocabulary.length; i++) {
                    if(testingFeatures[n][i] == 1) {
                        found = true;
                        wise += Math.log(dirichlet(vProbs.getCount(0, i, true), numWiseMessages));
                        predict += Math.log(dirichlet(vProbs.getCount(1, i, true), numPredictMessages));
                    }
                    // for some reason, makes prediction worse

                    // else {
                    //     wise += Math.log(dirichlet(vProbs.getCount(0, i, false), numWiseMessages) + Math.E);
                    //     predict += Math.log(dirichlet(vProbs.getCount(1, i, false), numPredictMessages) + Math.E);
                    // }
                }

                if(!found) {
                    wise += probNoVocabWise;
                    predict += probNoVocabPredict;
                }

                // determine which one is the most likely

                // correct label is still stored at the end
                if(wise >= predict) {
                    if(testingLabels[n] == 0)
                        correct++;
                }
                else {
                    if(testingLabels[n] == 1)
                        correct++;
                }
            }

            w.write("results: " + (double)((double)correct / (double)testingMessages.size()) + " accuracy" + endl);

            w.close();
        }
        catch(FileNotFoundException e1) {
            System.out.println("FileNotFoundException: " + e1.getMessage());
            e1.printStackTrace();
            System.exit(1);
        }
        catch(IOException e2) {
            System.out.println("IOException: " + e2.getMessage());
            e2.printStackTrace();
            System.exit(1);
        }
    }

    // only two options for a word being present, hence denom of 2
    private double dirichlet(int numerator, int denom) {
        return (double)((double)(numerator + 1) / (double)(denom + 2));
    }

    public static void main(String[] args) {
        if(args.length != 5) {
            System.out.println("Usage: ./naivebayes <stopwords> <trainingdata> <traininglabels> <testingdata> <testinglabels>");
            System.exit(1);
        }

        Classifier c = new Classifier(args[0], args[1], args[2], args[3], args[4]);
    }
}
