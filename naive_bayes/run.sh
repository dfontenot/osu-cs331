#!/bin/bash

if [ $# != 5 ]                  # display usage if wrong number of args
then
    java -jar naivebayes.jar
else
    java -jar naivebayes.jar "$1" "$2" "$3" "$4" "$5"
fi
